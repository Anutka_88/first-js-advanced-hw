class Employee {
constructor (name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
}

get name() {
    return this._name;
}
get age() {
    return this._age;
}
get salary() {
    return this._salary;
}
set name(name) {
    this._name = name;
}
set age(age) {
    this._age = age;
}
set salary(salary) {
    this._salary = salary;
}
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
get salary() {
    return this._salary *3;
}
get lang() {
    return this._lang;
}
}
const fill = new Programmer("Fill", 34, 500, ["English", "French"]);
const bob = new Programmer("Bob", 48, 800, ["English", "German"]);
console.log(fill);
console.log(bob);
console.log(fill.salary);

